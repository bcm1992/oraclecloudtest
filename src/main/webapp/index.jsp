<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*, javax.sql.*, javax.naming.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Oracle Cloud!</h1>
        <hr>
        <h2>Test Datasource javatrial4704db </h2>
        <form id="testForm" action="index.jsp" method="POST">Add new comment<br><input type="text" maxlength="256" name="comment"><input type="submit"></form><br><br>
            <%

                DataSource ds = null;
                Connection conn = null;
                ResultSet result = null;
                Statement stmt = null;
                PreparedStatement preparedStatement = null;
                ResultSetMetaData rsmd = null;
                try {
                    Context context = new InitialContext();
                    // Context envCtx = (Context) context.lookup("java:comp/env");
                    // ds =  (DataSource)envCtx.lookup("jdbc/Mysql/test");
                    ds = (DataSource) new InitialContext().lookup("javatrial4704db");
                    if (ds != null) {
                        conn = ds.getConnection();
                        stmt = conn.createStatement();
                        // add new commnent if any
                        if (request.getParameter("comment") != null) {
                            preparedStatement = conn.prepareStatement("insert into table1 ( col2 ) values (?) ");
                            preparedStatement.setString(1, request.getParameter("comment"));
                            preparedStatement.execute();
                            conn.commit();
                            preparedStatement.close();
                        }
                        //
                        result = stmt.executeQuery("select * from table1 order by col1 desc");
                    }
                } catch (SQLException e) {
                    System.out.println("Error occurred " + e);
                }
                int columns = 0;
                try {
                    rsmd = result.getMetaData();
                    columns = rsmd.getColumnCount();
                } catch (SQLException e) {
                    System.out.println("Error occurred " + e);
                }
            %>
        <table width="90%" border="1">
            <tr>
                <% // write out the header cells containing the column labels
                    try {
                        for (int i = 1; i <= columns; i++) {
                            out.write("<th>" + rsmd.getColumnLabel(i) + "</th>");
                        }
                %>
            </tr>
            <% // now write out one row for each entry in the database table
                    while (result.next()) {
                        out.write("<tr>");
                        for (int i = 1; i <= columns; i++) {
                            out.write("<td>" + result.getString(i) + "</td>");
                        }
                        out.write("</tr>");
                    }

                    // close the connection, resultset, and the statement
                    result.close();
                    stmt.close();
                    conn.close();
                } // end of the try block
                catch (SQLException e) {
                    System.out.println("Error " + e);
                } // ensure everything is closed
                finally {
                    try {
                        if (stmt != null) {
                            stmt.close();
                        }
                    } catch (SQLException e) {
                    }
                    try {
                        if (conn != null) {
                            conn.close();
                        }
                    } catch (SQLException e) {
                    }
                }

            %>

    </body>
</html>
